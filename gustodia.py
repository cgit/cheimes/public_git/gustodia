#!/usr/bin/env python2.7
from __future__ import print_function

import httplib
import json
import os
import socket
import sys

PREFIX = 'CUSTODIA_'
SECRET_PREFIX = PREFIX + 'SECRET_'
SOCK = './server_socket'

class HTTPUnixConnection(httplib.HTTPConnection):
    def __init__(self, socket_file):
        self.socket_file = socket_file
        httplib.HTTPConnection.__init__(self, 'localhost')

    def connect(self):
        sock = socket.socket(socket.AF_UNIX)
        sock.connect(self.socket_file)
        self.sock = sock


def get_custodia_envs():
    envs = {}
    for name, value in os.environ.iteritems():
        if name.startswith(SECRET_PREFIX):
            name = name[len(SECRET_PREFIX):]
            envs[name] = value
    return envs


def query_custodia(envs, sock=SOCK):
    conn = HTTPUnixConnection(sock)
    headers = {'REMOTE_USER': 'gustodia'}
    result = {}
    for name, path in envs.iteritems():
        conn.request(
            'GET', 'http://localhost/secrets/' + path, headers=headers
        )
        response = conn.getresponse()
        if response.status != 200:
            print(r1.status, r1.reason)
            continue
        value = json.loads(response.read())
        result[name] = value['value']
    return result


def filtered_env():
    env = {}
    for name, value in os.environ.iteritems():
        if not name.startswith(PREFIX):
            env[name] = value
    return env


def main():
    cenvs = get_custodia_envs()
    print('env vars', cenvs)
    secrets = query_custodia(cenvs)
    print('secrets from custodia', secrets)
    envp = filtered_env()
    envp.update(secrets)
    argv = sys.argv[1:]
    filename = argv[0]
    print("execve(%s, %s, %s)" % (filename, argv, envp))

if __name__ == '__main__':
    main()
