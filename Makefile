VENV=venv
PYTHON=python2.7
CUSTODIA=${VENV}/bin/custodia
REQUIREMENTS=cryptography jwcrypto git+https://github.com/simo5/custodia
CURL_CMD=curl --unix-socket server_socket -H "REMOTE_USER: user"

all: ${CUSTODIA} gustodia

${VENV}:
	virtualenv --python=${PYTHON} ${VENV}
	${VENV}/bin/pip install --upgrade pip setuptools

${CUSTODIA}: | ${VENV}
	${VENV}/bin/pip install ${REQUIREMENTS}

gustodia: gustodia.go
	go build $<

.PHONY=example
example: gustodia
	env -i EXAMPLE=foo CUSTODIA_SECRET_DB_PASSWORD=tests/mysecret \
	    ./gustodia ./entrypoint.sh apache

.PHONY=run_server
run_server: ${CUSTODIA}
	${CUSTODIA}

.PHONY=init_secret
init_secret:
	${CURL_CMD} -X POST http://localhost/secrets/tests/
	${CURL_CMD} -H "Content-Type: application/json" \
	    -X PUT \
	    -d '{"type": "simple", "value": "SuperSecretPassword"}' \
	    http://localhost/secrets/tests/mysecret

.PHONY=upgrade
upgrade: | ${VENV}
	${VENV}/bin/pip install --upgrade ${REQUIREMENTS}

.PHONY=clean
clean:
	@rm -rf ${VENV}
	@rm -rf secrets.db
	@rm -f custodia.audit.log
	@rm -f gustodia
	@rm -f server_socket
